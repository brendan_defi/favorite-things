Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
Deep purple, lapis lazuli, if you will.

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
Ramen and soba noodles. Katsu-kare as well. Most anything Japanese.

3. Who is your favorite fictional character?
Gon from Hunter x Hunter and Musashi from Vagabond.

4. What is your favorite animal?
Sapiens. Then elephants. Then dogs.

5. What is your favorite programming language? (Hint: You can always say Python!!)
Javascript.
